# from __future__ import print_function, division
import numpy as np, cv2, sys, os, math, random, time, torch, torch.nn as nn, collections, tensorboardX, gym
from collections import deque


Experience = collections.namedtuple("Experience", ("obs", "act", "rew", "nobs", "done"))

class ReplayBuffer(deque):
    def __init__(self,*args):
        super().__init__(*args)
        self.pop()

    def add(self,obs,act,rew,nobs,done):
        self.append(Experience(obs,act,rew,nobs,done))

class Agent(nn.Module):
  def __init__(self, task, live, params):
    super(Agent, self).__init__()
    self.task      = task
    self.live      = live
    self.params    = params
    self.obs_space = task.obs_space
    self.act_space = task.act_space
    self.discrete  = isinstance(self.act_space, gym.spaces.discrete.Discrete)
  #--------------------------------------------------------------------------
  def agent_state_dict(self):
    d = {}
    for k,v in self.__dict__.items():
      try: d[k] = v.state_dict()
      except: pass
    return d
  #--------------------------------------------------------------------------
  def agent_load_state_dict(self, d):
    for k,v in d.items():
      self.__dict__[k].load_state_dict(v)
  #--------------------------------------------------------------------------
  def update_target(self, online, target, tau):
    for po,pt in zip(online.parameters(), target.parameters()): pt.data = tau*po.data.clone() + (1-tau)*pt.data.clone()
  #----------------------------------------------------------------------------
  def reset(self): return {}
  #----------------------------------------------------------------------------
  def act(self, obs):
    act = np.random.random(self.act_space.shape).astype(np.float32)*2-1
    return act
  #----------------------------------------------------------------------------
  def train(self, buf): return {}
  #----------------------------------------------------------------------------
  def batch(self, buf):
    batch = Experience(*map(lambda x: torch.FloatTensor(x).view(self.params.BATCH,-1).cuda(), zip(*random.sample(buf, self.params.BATCH))))
    # batch = self.task.offline(batch)
    return batch

class SuperDIAYNAgent(Agent):
  def __init__(self, task, live, params):
    super(SuperDIAYNAgent, self).__init__(task, live, params)

    self.model  = nn.Sequential(nn.Linear(self.obs_space.shape[0]+self.act_space.shape[0], 300), nn.ReLU(), nn.Linear(300, 400), nn.ReLU(), nn.Linear(400, self.obs_space.shape[0]))
    self.pi_mu  = nn.Sequential(nn.Linear(self.obs_space.shape[0]+self.params.L_SIZE,      300), nn.ReLU(), nn.Linear(300, 400), nn.ReLU(), nn.Linear(400, self.act_space.shape[0]))
    self.pi_sig = nn.Sequential(nn.Linear(self.obs_space.shape[0]+self.params.L_SIZE,      300), nn.ReLU(), nn.Linear(300, 400), nn.ReLU(), nn.Linear(400, self.act_space.shape[0]), nn.Softplus())
    self.dh     = nn.Sequential(nn.Linear(self.obs_space.shape[0]+self.act_space.shape[0], 300), nn.ReLU(), nn.Linear(300, 400), nn.ReLU(), nn.Linear(400, 1))
    self.disc   = nn.Sequential(nn.Linear(self.obs_space.shape[0],                         300), nn.ReLU(), nn.Linear(300, 400), nn.ReLU(), nn.Linear(400, self.params.L_SIZE))

    self.cuda()

    self.opt_m  = torch.optim.Adam(self.model.parameters(), lr=self.params.LR_M)
    self.opt_pi = torch.optim.Adam(list(self.pi_mu.parameters()) + list(self.pi_sig.parameters()), lr=self.params.LR_P)
    self.opt_dh = torch.optim.Adam(self.dh   .parameters(), lr=self.params.LR_DH)
    self.opt_d  = torch.optim.Adam(self.disc .parameters(), lr=self.params.LR_D)

    self.J = self.params.J_SCALE * self.act_space.shape[0]
    self.skills_used = [] # hacky way to keep track of skill labels in the buffer

  #------------------------------------------------------------------------------

  def reset(self):
    self.skill = np.random.randint(self.params.L_SIZE)
    return {"skill": self.skill}

  #----------------------------------------------------------------------------

  def sample_action(self, obs, latent=None):
    if latent is None:
      latent = np.zeros((obs.shape[0], self.params.L_SIZE), np.float32)
      latent[:,self.skill] = 1
      latent = torch.from_numpy(latent).cuda()
    obs  = torch.cat([obs, latent], dim=1)

    mu   = self.pi_mu (obs)
    sig  = self.pi_sig(obs)
    dist = torch.distributions.Normal(loc=mu, scale=sig+1e-3)
    lgt  = dist.rsample()
    act  = torch.tanh(lgt)
    cor  = -torch.sum(torch.log(1 - act**2 + 1e-5), dim=1) # change of variables for PDF of tanh(gaussian)
    cor  = cor.unsqueeze(dim=1)
    lpi  = dist.log_prob(lgt) + cor
    lpi  = torch.sum(lpi, dim=1)
    return act, lpi

  #----------------------------------------------------------------------------

  def train_done(self, buf):
    batch = self.batch(buf)

    done_pred = self.dh(torch.cat([batch.obs, batch.act], dim=1))
    loss = torch.nn.functional.binary_cross_entropy_with_logits(done_pred, batch.done)

    self.opt_dh.zero_grad()
    loss.backward()
    self.opt_dh.step()

    return {"loss_dh": loss.detach()}

  #----------------------------------------------------------------------------

  def train_policy(self, buf):
    batch = self.batch(buf)

    loss_diversity = torch.zeros([]).float().cuda()
    loss_entropy   = torch.zeros([]).float().cuda()
    loss_spin      = torch.zeros([]).float().cuda()
    discount       = torch.ones([batch.obs.shape[0]]).float().cuda()

    obs = batch.obs
    skill = np.random.randint(self.params.L_SIZE)
    latent = np.zeros((batch.obs.shape[0], self.params.L_SIZE), np.float32)
    latent[:,skill] = 1.0
    latent = torch.from_numpy(latent).cuda()
    label  = np.full((batch.obs.shape[0],), skill, np.int64)
    label  = torch.from_numpy(label).cuda()
    for t in range(self.params.HORIZON):
      act, lpi = self.sample_action(obs, latent)

      obs_act = torch.cat([obs, act], dim=1)

      nobs_h = obs + self.model(obs_act)

      disc_lgt  = self.disc(nobs_h)
      diversity = torch.nn.functional.cross_entropy(disc_lgt, label, reduction="none")

      # compute the discounted losses
      loss_diversity = loss_diversity + self.params.GAMMA**t * diversity # we want diversity to be low but don't want to end the episode, so we don't backprop into the done estimate for this
      loss_spin      = loss_spin      + discount * self.params.SPIN_COST * torch.sum(act**2, dim=1)
      loss_entropy   = loss_entropy   + discount * self.params.ALPHA * lpi[None,:] # add a "model_id" dimension

      # estimate whether this action would end the episode, and discount accordingly
      done_h = torch.sigmoid(self.dh(obs_act)).squeeze(dim=1)[None,:] # add a "model_id" dimension
      discount = discount * self.params.GAMMA * (1 - done_h)

      obs = nobs_h

    # combine all timesteps of loss together, plus action regularizer
    loss_diversity  = torch.mean(loss_diversity)
    loss_spin       = torch.mean(loss_spin)
    loss_entropy    = torch.mean(loss_entropy)
    loss = loss_diversity + loss_spin + loss_entropy
    loss = loss / self.params.HORIZON

    self.opt_pi.zero_grad()
    loss.backward()
    self.opt_pi.step()

    return {"loss_pi"          : loss          .detach(),
            "loss_pi_diversity": loss_diversity.detach(),
            "loss_pi_spin"     : loss_spin     .detach(),
            "loss_pi_entropy"  : loss_entropy  .detach()}

  #----------------------------------------------------------------------------

  def train(self, buf):
    if len(buf) < self.params.BATCH: return {}

    ms_metrics = self.train_model (buf)
    dh_metrics = self.train_done  (buf)
    pi_metrics = self.train_policy(buf)
    dc_metrics = self.train_disc  (buf)
    metrics = dict(ms_metrics,**dh_metrics)
    for met in (pi_metrics, dc_metrics):
        metrics.update(met)

    return metrics

  #----------------------------------------------------------------------------

  def train_model(self, buf):
    batch = self.batch(buf)

    nobs_pred = batch.obs + self.model(torch.cat([batch.obs, batch.act], dim=1))
    loss = torch.mean((nobs_pred - batch.nobs)**2)

    self.opt_m.zero_grad()
    loss.backward()
    self.opt_m.step()

    return {"loss_m": loss.detach()}

  #------------------------------------------------------------------------------

  def _batch_with_skills(self, buf):
    indices = np.random.randint(0, len(buf), (self.params.BATCH,))
    batch   = Experience(*map(lambda x: torch.FloatTensor(x).view(self.params.BATCH,-1).cuda(), zip(*[buf[i] for i in indices])))
    # batch   = self.task.offline(batch)
    skills  = np.array([self.skills_used[i] for i in indices], dtype=np.int64)
    skills  = torch.from_numpy(skills).cuda()
    return batch, skills

  #------------------------------------------------------------------------------

  def train_disc(self, buf):
    batch, skills = self._batch_with_skills(buf)

    disc_pred = self.disc(batch.nobs)
    loss = torch.nn.functional.cross_entropy(disc_pred, skills)

    self.opt_d.zero_grad()
    loss.backward()
    self.opt_d.step()

    return {"loss_disc": loss.detach()}

  #--------------------------------------------------------------------------

  def act(self, obs):
    obs = torch.from_numpy(obs).float().view(1,-1).cuda()
    act, lpi = self.sample_action(obs)
    act = act.detach().squeeze().cpu().numpy()
    self.skills_used.append(self.skill)
    return act


import gym, numpy as np

class SparseHalfCheetah:
    def __init__(self,**kwargs):
        self.env = gym.make('HalfCheetah-v2')
        self.__dict__.update(self.env.__dict__)
        self.sparseRange = [1,np.inf] if 'sparseRange' not in kwargs else kwargs['sparseRange']
    
    def reset(self, *kwargs):
        return self.env.reset(*kwargs)

    def step(self,act):
        obs, reward, done, other = self.env.step(act)
        reward = 1.0 if reward > self.sparseRange[0] and reward < self.sparseRange[1] else 0.0
        return obs, reward, done, other

    def render(self, *kwargs):
        return self.env.render(*kwargs)
    

import torch, sys, cv2, numpy as np, matplotlib.pyplot as plt
from PointMass import PointMass

def arg(tag, default):
    if tag in sys.argv:
        val = sys.argv[sys.argv.index(tag)+1]
    else:
        val = default
    return val

path = arg('--file',None)
params = torch.load(path)
df = params['trainer/df']
print(params.keys())
agent = params['trainer/policy']
nOptions = len(df.forward(torch.Tensor([[0,0]]))[0])

w,h = 600,600
scale = 2
brush_size = 2

img = cv2.resize(cv2.cvtColor(np.random.randint(225,256,(h,w)).astype(np.uint8), cv2.COLOR_GRAY2BGR), dsize=(w,h), interpolation=cv2.INTER_NEAREST)
cmap = plt.get_cmap('jet')
network = arg('--network','disc')
if network == 'disc':
    for x in range(w):
        for y in range(h):
            out = df.forward(torch.Tensor([[x/w*scale,y/h*scale]]))
            n = torch.argmax(out)
            slice = np.array(cmap(int(n/nOptions*256))[:3])*256
            img[x,y] = slice.astype(int)
elif network == 'agent':
    env = params['exploration/env']
    for n in range(nOptions):
        
        for eps in range(20):
            obs = env.reset()
            for i in range(50):
                latent = [0 for _ in range(nOptions)]
                latent[n] = 1
                obs = torch.Tensor(np.concatenate([obs,np.array(latent)]))
                # print(obs)
                act = np.array(agent.forward(obs,deterministic=True)[0].detach())
                obs, _, _, _ = env.step(act)
                x = min(int(obs[0]/scale*w),w-1)
                y = min(int(obs[1]/scale*h),h-1)
                slice = np.array(cmap(int(n/nOptions*256))[:3])*256
                xmin,xmax = (max(0,x-brush_size),min(w-1,x+brush_size))
                ymin,ymax = (max(0,y-brush_size),min(h-1,y+brush_size))
                img[xmin:xmax,ymin:ymax] = slice.astype(int)
    

cv2.imshow('map',img)
cv2.waitKey(0)


import sys, gym, json, torch, numpy as np

import hydra
from rlkit.envs.wrappers import NormalizedBoxEnv
import torch.nn.functional as F

def arg(tag, default=None):
    if tag in sys.argv:
        return sys.argv[sys.argv.index(tag)+1]
    else: 
        return default


def print_trajectory(trajectory, print_length = 100):
    print_string = ''
    increment = max(1,int(len(trajectory)/print_length))
    for n in range(0,len(trajectory),increment):
        nex = min(n+increment,len(trajectory)-1)
        val = np.mean(np.array(trajectory[n:nex]))
        if val == 1:
            print_string += 'T'
        elif val == 0:
            print_string += '_'
        else:
            print_string += '-'
    print(print_string)

    


path = arg('--path')
show = arg('--show',False)
interactive = arg('--interactive', False)
with open(path+'/variant.json','r') as f:
    variant = json.load(f)

env_name = variant['env']

try:
    env = NormalizedBoxEnv(gym.make(env_name))
except:
    env = {'class':env_name}
    env = NormalizedBoxEnv(hydra.utils.instantiate(env))

params = torch.load(path+'/params/params.pkl')
policy = params['trainer/policy']
disc = params['trainer/df']

skill_dim = variant['skill_dim']

if interactive:
    inter = False
    key = None
    while not inter:
        lastKey = key
        key = input("Choose skill to show (0-{}):".format(skill_dim-1))
        if key == '':
            key = lastKey
        try:
            skill = int(key)
        except:
            pass
        if key == 'q':
            inter = True
        elif skill < skill_dim and skill >= 0:
            done = False
            print('Skill: ',key)
            latent = [0 for _ in range(skill_dim)]
            latent[skill] = 1
            obs = env.reset()
            step = 0
            accuracy = []
            while not done:
                obs = torch.Tensor(np.concatenate([obs,np.array(latent)]))
                act = np.array(policy.forward(obs,deterministic=False)[0].detach())
                obs, _, done, _ = env.step(act)
                discrim = disc.forward(torch.Tensor([obs]))
                soft_d = F.log_softmax(discrim, 1)
                accuracy.append(torch.argmax(discrim) == skill)
                env.render()
                # print(step,obs)
                step += 1
                if step > variant['algorithm_kwargs']['max_path_length'] or step > 200:
                    done = True
            print_trajectory(accuracy)
            print(np.mean(accuracy))
        else:
            print("Bad input skill")
else:

    accuracy = [[] for _ in range(skill_dim)]
    for n in range(skill_dim):
        print('Skill: ',n)
        obs = env.reset()
        latent = [0 for _ in range(skill_dim)]
        latent[n] = 1
        
        done = False
        
        # print(n)
        step = 0
        while not done:
            obs = torch.Tensor(np.concatenate([obs,np.array(latent)]))
            act = np.array(policy.forward(obs,deterministic=False)[0].detach())
            obs, _, done, _ = env.step(act)
            discrim = disc.forward(torch.Tensor([obs]))
            soft_d = F.log_softmax(discrim, 1)
            accuracy[n].append(torch.argmax(discrim) == n)
            # print(torch.argmax(discrim),": ",soft_d[0][torch.argmax(discrim)])
            if show:
                env.render()
            step += 1
            if step > variant['algorithm_kwargs']['max_path_length']:
                done = True
        print_trajectory(accuracy[n])
        print(np.mean(accuracy[n]))

    print("Total accuracy: ", np.mean([np.mean(a) for a in accuracy]))


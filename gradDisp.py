import torch, re
from rlkit.torch.networks import FlattenMlp

def displayGradientInfo(tensor, name = "Tensor"):
    if tensor.grad_fn is None:
        print(f"{name} has no gradient")
    else:
        print(f"{name}:")
        gradTrace(tensor.grad_fn)

def gradTrace(grad_fn,leading = "|->",last = True):
    name = re.findall('\'.*\'',str(grad_fn.__class__))[0][1:-1]
    print(f"{leading}{name}")
    if last:
        n_leading = f"{leading[:-3]}   |->"
    else:
        n_leading = f"{leading[:-2]}  |->"
    if "next_functions" in dir(grad_fn) and len(grad_fn.next_functions) > 0:
        
        for n, n_grad_fn in enumerate(grad_fn.next_functions):
            if n < len(grad_fn.next_functions)-1:
                last = False
            else:
                last = True
            gradTrace(n_grad_fn[0],n_leading, last)


if __name__ == "__main__":
    q = FlattenMlp([10,10],1,4)
    a = torch.Tensor([[1,2,3,4]])
    a = q(a)
    displayGradientInfo(a)
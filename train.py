import hydra, os, time, utils, torch.nn as nn
import dmc2gym

from logger import Logger
from DIAYN import SuperDIAYNAgent, Experience, ReplayBuffer


def make_env(cfg):
    """Helper function to create dm_control environment"""
    if cfg.env == 'ball_in_cup_catch':
        domain_name = 'ball_in_cup'
        task_name = 'catch'
    else:
        domain_name = cfg.env.split('_')[0]
        task_name = '_'.join(cfg.env.split('_')[1:])

    env = dmc2gym.make(domain_name=domain_name,
                       task_name=task_name,
                       seed=cfg.seed,
                       visualize_reward=True)
    env.seed(cfg.seed)
    assert env.action_space.low.min() >= -1
    assert env.action_space.high.max() <= 1

    return env

class Task(nn.Module):
  def __init__(self, obs_space, act_space, params):
    super(Task, self).__init__()
    self.obs_space = obs_space
    self.act_space = act_space
    self.params    = params
  def reset(self): return {}
  def obs(self, obs): return obs
  def online(self, step): return step
  def offline(self, batch): return batch
  def decode(self, step): return step
  def batch(self, buf): return Experience(*map(lambda x: torch.FloatTensor(x).view(self.params.BATCH,-1).cuda(), zip(*random.sample(buf, self.params.BATCH))))
  def train(self, buf): return {}


class Workspace:
    def __init__(self,config):
        self.work_dir = os.getcwd()
        self.cfg = config
        self.env = make_env(config)
        # agentConfig = {'task': self.env,
        #                'live': self.cfg.agent.params.live,
        #                'params': }
        task = Task(self.env.observation_space,self.env.action_space,self.cfg.agent.params)
        self.agent = SuperDIAYNAgent(task,False, self.cfg.agent.params.params)

        self.logger = Logger(self.work_dir,
                             save_tb=config.log_save_tb,
                             log_frequency=config.log_frequency,
                             agent=config.agent.name)

        self.step = 0

        self.replay_buffer = ReplayBuffer(self.cfg.replay_buffer.params)


    def evaluate(self):
        timer = time.time()
        average_episode_reward = 0
        overall_reward = 0
        for episode in range(self.cfg.num_eval_episodes):
            obs = self.env.reset()
            self.agent.reset()
            # self.video_recorder.init(enabled=(episode == 0))
            done = False
            episode_reward = 0
            env_info = None
            while not done:
                with utils.eval_mode(self.agent):
                    action = self.agent.act(obs, sample=False, env_info=env_info)
                obs, reward, done, env_info = self.env.step(action)
                # if self.cfg.render:
                #     self.video_recorder.record(self.env)
                episode_reward += reward
                overall_reward += env_info['overallReward']

            average_episode_reward += episode_reward
            # if self.cfg.render:
            #     self.video_recorder.save(f'{self.step}.mp4')
        self.logger.log('eval/duration', time.time()-timer,self.step)
        average_episode_reward /= self.cfg.num_eval_episodes
        self.logger.log('eval/episode_reward', average_episode_reward,
                        self.step)
        overall_reward /= self.cfg.num_eval_episodes
        self.logger.log('eval/overall_reward', overall_reward,
                        self.step)
        self.logger.dump(self.step)
        self.agent.save_model(self.logger, self.step)

    def run(self):
        last_eval = 0
        episode, episode_reward, done = 0, 0, True
        env_infos = []
        start_time = time.time()
        while self.step < self.cfg.num_train_steps:
            if done:
                if self.step > 0:
                    self.logger.log('train/duration',
                                    time.time() - start_time, self.step)
                    start_time = time.time()
                    self.logger.dump(
                        self.step, save=(self.step > self.cfg.num_seed_steps))

                    self.logger.log('timing/episode',time.time()-ep_time,self.step)
                    self.logger.log('timing/action',action_times,self.step)
                    self.logger.log('timing/env_step',env_times,self.step)
                    self.logger.log('timing/train',update_times,self.step)

                    # self.logger.log('train/length', episode_step,self.step)

                    
             

                # evaluate agent periodically
                if self.step > 0 and self.step >= last_eval + self.cfg.eval_frequency:
                    last_eval = self.step
                    self.logger.log('eval/episode', episode, self.step)
                    self.evaluate()

                self.logger.log('train/episode_reward', episode_reward,
                                self.step)

                obs = self.env.reset()
                self.agent.reset()
                done = False
                episode_reward = 0
                episode_step = 0
                env_infos = []
                episode += 1


                action_times = 0
                update_times = 0
                env_times = 0
                ep_time = time.time()

                self.logger.log('train/episode', episode, self.step)

            # sample action for data collection
            if self.step < self.cfg.num_seed_steps:
                action = self.env.action_space.sample()
            else:
                timer = time.time()
                action = self.agent.act(obs)
                action_times += (time.time() - timer)
                    

            # run training update
            if self.step >= self.cfg.num_seed_steps:
                timer = time.time()
                metrics = self.agent.train(self.replay_buffer)
                update_times += (time.time() - timer)
            
            for metric in metrics:
                self.logger.log(f'train/{metric}',metrics[metric],self.step)

            timer = time.time()
            next_obs, reward, done, env_info = self.env.step(action)
            env_times += (time.time() - timer)

            env_infos.append(env_info)


            # allow infinite bootstrap
            done = float(done)
            done_no_max = 0 if episode_step + 1 == self.env._max_episode_steps else done
            episode_reward += reward

            self.replay_buffer.add(obs, action, reward, next_obs, done)

            obs = next_obs
            episode_step += 1
            self.step += 1




@hydra.main(config_path='config/base.yaml', strict=False)
def main(cfg):
    workspace = Workspace(cfg)
    workspace.run()

if __name__ == "__main__":
    main()